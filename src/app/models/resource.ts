import {ResourceStates} from './enums/resource-states.enum';
import {User} from './user';
import {Categories} from './enums/categories.enum';
import {IEditable} from './i-editable';
import {Comment} from './comment';
import {Favorite} from './favorite';
import {ResourceLike} from './resource-like';

export class Resource implements IEditable {

  constructor() {
  }

  updatedAt: Date;
  createdAt: Date;
  id: number;
  title: string;
  state: ResourceStates;
  viewCount: number;
  likeCount: number;
  searchCount: number;
  image: any;
  Resource_images: Array<any>;
  User: User;
  position: string;
  description: string;
  isEnabled: boolean;
  Comments: Comment[];
  category: string;
  userInvited: User[];
  eventPlace: string;
  eventDate: Date;
  ResourceCategoryId: number;
  Resource_likes: ResourceLike[];
}
