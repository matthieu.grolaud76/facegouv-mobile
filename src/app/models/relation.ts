import {User} from './user';

export class Relation {
  id: number;
  SenderId: number;
  Sender: User;
  ReceiverId: number;
  Receiver: User;
  isConfirmed: boolean;
}

export enum RelationSates {
  NotAdded,
  NotConfirmed,
  Confirmed
}
