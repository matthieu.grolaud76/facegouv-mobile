import {User} from './user';
import {Resource} from './resource';

export class ResourceShared {
  id: number;
  Sender: User;
  Receiver: User;
  Resource: Resource;
  isViewed;
}
