import {IEditable} from './i-editable';
import {IComment} from './i-comment';
import {User} from './user';

export class Answer implements IComment {
  constructor(editor: User) {
    this.User = editor;
  }

  content: string;
  User: User;

  updatedAt: Date;
  createdAt: Date;
  id: number;
}
