export enum UserStates {
  banned = 1,
  visitor = 2,
  verified = 3,
  moderator = 4,
  admin = 5,
  superAdmin = 6
}
