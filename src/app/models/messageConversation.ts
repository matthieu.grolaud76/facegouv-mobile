import {User} from './user';

export class MessageConversation {
  constructor(user: User, conversationId: number) {
    this.User = user;
    this.ConversationId = conversationId;
  }

  User: User;
  ConversationId: number;
  Message: string;
  SeenByIds: JSON;
  hasBeenDeleted: boolean;
}
