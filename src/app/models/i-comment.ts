import {User} from './user';
import {IEditable} from './i-editable';

export interface IComment extends IEditable{
  User: User;
  content: string;
}
