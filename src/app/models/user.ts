import {UserStates} from './enums/user-states.enum';
import {IEditable} from './i-editable';
import {Favorite} from './favorite';
import {Relation} from './relation';

export class User implements IEditable {
  constructor() {

  }

  updatedAt: Date;
  createdAt: Date;
  id: number;

  lastName: string;
  firstName: string;
  password: string;
  age: number;
  email: string;
  tel: string;
  postalCode: string;
  userStateId: UserStates;
  isEnabled: boolean;
  profilePicture: string;

  favorites: Favorite[];
  friends: Relation[];
}
