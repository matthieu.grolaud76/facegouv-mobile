import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {AuthService} from '../../services/auth.service';
import {faUserPlus, faUnlock, faUserEdit, faSignOutAlt, faUsers} from '@fortawesome/free-solid-svg-icons';
import {Global} from '../../global';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  //region Icons
  faUserPlus = faUserPlus;
  faUnlock = faUnlock;
  faUserEdit = faUserEdit;
  faSignOutAlt = faSignOutAlt;
  faUsers = faUsers;
  //endregion Icons

  user: User;

  constructor(private authService: AuthService, private userService: UserService, public global: Global, private appEventManagerService: AppEventManagerService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToUser();
    this.user = this.authService.user;
    this.appEventManagerService.subscribe('userUpdated', async () => {
      await this.userService.getUserById(this.user.id);
    });
  }

  async logout(): Promise<void> {
    await this.authService.logout();
    this.user = null;
  }

  subscribeToUser(): void {
    this.userService.userSubject.subscribe(
      (user: User) => {
        this.user = user;
      },
      error => {
        console.error(error);
      }
    );
  }
}
