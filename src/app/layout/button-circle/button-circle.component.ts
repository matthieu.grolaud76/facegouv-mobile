import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button-circle',
  templateUrl: './button-circle.component.html',
  styleUrls: ['./button-circle.component.scss']
})
export class ButtonCircleComponent implements OnInit {
  @Input() icon;

  constructor() {
  }

  ngOnInit(): void {
  }

}
