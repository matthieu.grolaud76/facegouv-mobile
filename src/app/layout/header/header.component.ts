import {Component, Input, OnInit} from '@angular/core';
import {faUser, faBars} from '@fortawesome/free-solid-svg-icons';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {MessageFriendsComponent} from '../../components/message-friends/message-friends.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  //#region icons
  faUser = faUser;
  faBars = faBars;
  //#endregion

  @Input() title: string;
  closeResult = '';
  isConnected: boolean;

  constructor(private eventManager: AppEventManagerService, public dialog: MatDialog,
              public authService: AuthService) {
  }

  ngOnInit(): void {
  }

  toggleNavbar(): void {
    this.eventManager.broadcast('toggleNavbarEvent');
  }

  open(): void {
    this.dialog.open(MessageFriendsComponent, {
      maxWidth: '95vw',
      width: '95vw',
    });
  }
}
