import {Component, OnInit} from '@angular/core';
import {ResourcesService} from '../../services/resources.service';
import {Favorite} from '../../models/favorite';
import {AuthService} from '../../services/auth.service';
import {FavoriteService} from '../../services/favorite.service';
import {Resource} from '../../models/resource';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  favorites: Favorite[];
  resources: Resource[] = new Array<Resource>();

  constructor(private favoriteService: FavoriteService, private authService: AuthService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToFavorites();
    await this.favoriteService.getAllFavorites(this.authService.user.id);
  }

  subscribeToFavorites(): void {
    this.favoriteService.favoritesSubject.subscribe(
      (favorites: Array<Favorite>) => {
        this.favorites = favorites;
        for (const favorite of favorites) {
          this.resources.push(favorite.Resource);
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
