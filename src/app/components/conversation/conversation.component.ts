import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {MessageConversation} from '../../models/messageConversation';
import {ConversationService} from '../../services/conversation.service';
import {AuthService} from '../../services/auth.service';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {Conversation} from '../../models/conversation';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss']
})
export class ConversationComponent implements OnInit {
  conversation: Conversation;
  @Input() conversationId: number;
  isDeployed: boolean;
  newMessage: MessageConversation;
  hasNotification: boolean;

  constructor(private conversationService: ConversationService, public authService: AuthService,
              private appEventManagerService: AppEventManagerService) {
  }

  async ngOnInit(): Promise<void> {
    this.newMessage = new MessageConversation(this.authService.user, this.conversationId);
    this.subscribeToConversation();
    this.appEventManagerService.subscribe('conversationChanged', () => {
      this.conversationService.getConversation(this.conversationId);
      if (!this.isDeployed) {
        this.hasNotification = true;
      }
    });
    await this.conversationService.getConversation(this.conversationId);
  }

  subscribeToConversation(): void {
    this.conversationService.conversationSubject.subscribe(value => {
      this.conversation = value;
    });
  }

  toggleConversation(): void {
    this.isDeployed = !this.isDeployed;
    this.hasNotification = false;
  }

  async sendMessage(): Promise<void> {
    await this.conversationService.addMessage(this.newMessage);
    this.newMessage = new MessageConversation(this.authService.user, this.conversationId);
  }
}
