import {Component, OnInit} from '@angular/core';
import {faTimes} from '@fortawesome/free-solid-svg-icons';
import {User} from '../../../models/user';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {
  //#region icons
  faTimes = faTimes;
  //#endregion
  user: User;
  confirmPassword: string;

  constructor(private authService: AuthService) {
  }

  signIn(): void {
    this.authService.signIn(this.user, this.confirmPassword);
  }

  ngOnInit(): void {
    this.user = new User();
  }
}
