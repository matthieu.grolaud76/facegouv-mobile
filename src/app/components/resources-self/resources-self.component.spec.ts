import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesSelfComponent } from './resources-self.component';

describe('ResourcesSelfComponent', () => {
  let component: ResourcesSelfComponent;
  let fixture: ComponentFixture<ResourcesSelfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourcesSelfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesSelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
