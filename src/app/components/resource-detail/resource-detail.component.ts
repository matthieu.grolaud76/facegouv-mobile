import {Component, Input, OnInit} from '@angular/core';
import {Resource} from '../../models/resource';
import {ActivatedRoute} from '@angular/router';
import {Comment} from '../../models/comment';
import {Subscription} from 'rxjs';
import {ResourcesService} from '../../services/resources.service';
import {CommentTypes} from '../../models/enums/comment-types.enum';
import {faThumbsUp, faShareAlt, faEye} from '@fortawesome/free-solid-svg-icons';
import {AuthService} from '../../services/auth.service';
import {Favorite} from '../../models/favorite';
import {FavoriteService} from '../../services/favorite.service';
import {Global} from '../../global';
import {AppEventManagerService} from "../../services/app-event-manager.service";
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-resource-detail',
  templateUrl: './resource-detail.component.html',
  styleUrls: ['./resource-detail.component.scss']
})
export class ResourceDetailComponent implements OnInit {

  //region icons

  faThumbsUp = faThumbsUp;
  faEye = faEye;
  faShareAlt = faShareAlt;

  //endregion icons

  user:User;
  resource: Resource;
  resourceSubscription: Subscription;
  comment: Comment;
  subCommentsDeployed: number[];
  commentTypes: typeof CommentTypes = CommentTypes;
  popupShareIsOpen: boolean;

  constructor(private route: ActivatedRoute, private resourcesService: ResourcesService,
              private favoriteService: FavoriteService, private authService: AuthService,
              public global: Global, private appEventManager: AppEventManagerService) {
  }

  async ngOnInit(): Promise<void> {
    const id = this.route.snapshot.paramMap.get('id');
    this.user = this.authService.user;
    const parsedId = parseInt(id, 10);
    this.subCommentsDeployed = new Array<number>();
    this.subscribeToResource();
    await this.resourcesService.incrementView(parsedId);
    await this.resourcesService.getResourceById(parsedId);
    if (this.user) {
      await this.favoriteService.getAllFavorites(this.authService.user.id);
    }
    await this.appEventManager.subscribe('commentPosted', () => {
      setTimeout(() => {
        this.resourcesService.getResourceById(parsedId);
      }, 100);
    });
    this.appEventManager.subscribe('userConnected', () => {this.user = this.authService.user})
    this.appEventManager.subscribe('userDisconnected', () => {this.user = null})
  }

  subCommentsVisible(id: number): boolean {
    return this.subCommentsDeployed.includes(id);
  }

  subscribeToResource(): void {
    this.resourceSubscription = this.resourcesService.resourceSubject.subscribe(
      (resource: Resource) => {
        this.resource = resource;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  async ToggleFavorite(): Promise<void> {
    if (this.user) {
      const fav = this.authService.user.favorites.find((f: Favorite) => f.Resource.id === this.resource.id);
      if (fav) {
        await this.favoriteService.removeFavorite(fav.id);
      } else {
        await this.favoriteService.addToFavorite(this.authService.user.id, this.resource.id);
      }
    }
  }

  async likeResource(): Promise<void> {
    if (this.user) {
      await this.resourcesService.likeResource(this.authService.user.id, this.resource.id);
    }
  }

  shareResource(): void {

  }

  isFavorite(): boolean {
    if (this.authService.user && this.authService.user.favorites && this.resource) {
      return this.resourcesService.isFavorite(this.authService.user.favorites, this.resource.id);
    }
  }

  isLiked(): boolean {
    if (this.resource?.Resource_likes && this.authService.user) {
      return this.resourcesService.isLiked(this.resource.Resource_likes, this.authService.user.id);
    }
  }

  openSharePopup(): void {
    if (this.user) {
      this.popupShareIsOpen = !this.popupShareIsOpen;
    }
  }
}
