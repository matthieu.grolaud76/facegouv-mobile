import {Component, OnInit} from '@angular/core';
import {ResourcesService} from '../../services/resources.service';
import {Category} from '../../models/category';
import {Resource} from '../../models/resource';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-resource-form',
  templateUrl: './resource-form.component.html',
  styleUrls: ['./resource-form.component.scss']
})
export class ResourceFormComponent implements OnInit {

  categories: Category[] = [
    {id: 1, type: 'Publication'},
    {id: 2, type: 'Activité'},
    {id: 3, type: 'Événement'}
  ];

  isEvent;
  image;
  newResource: Resource;

  constructor(private resourceService: ResourcesService, private authService: AuthService) {
    this.newResource = new Resource();
  }

  category;

  ngOnInit(): void {
  }

  async submitResource(): Promise<void> {
    await this.resourceService.postResource(this.authService.user.id, this.newResource, this.image);
  }

  uploadFile(event): void {
    this.image = event.target.files[0];
  }

}
