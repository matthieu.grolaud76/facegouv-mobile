import {Component, Input, OnInit} from '@angular/core';
import {Resource} from '../../models/resource';
import {Router} from '@angular/router';
import {faThumbsUp, faShareAlt, faEye} from '@fortawesome/free-solid-svg-icons';
import {Global} from "../../global";

@Component({
  selector: 'app-resource-card',
  templateUrl: './resource-card.component.html',
  styleUrls: ['./resource-card.component.scss']
})
export class ResourceCardComponent implements OnInit {

  //region icons

  faThumbsUp = faThumbsUp;
  faEye = faEye;
  faShareAlt = faShareAlt;

  //endregion icons
  @Input() resource: Resource;
  @Input() limit = 100;

  constructor(private router: Router, public global: Global) {
  }

  ngOnInit(): void {
  }

  async goToResource(): Promise<void> {
    await this.router.navigate(['/resource-detail', this.resource.id]);
  }
}
