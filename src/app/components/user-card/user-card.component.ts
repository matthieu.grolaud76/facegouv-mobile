import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {User} from '../../models/user';
import {UserService} from '../../services/user.service';
import {AuthService} from '../../services/auth.service';
import {Relation, RelationSates} from '../../models/relation';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {Global} from '../../global';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit, OnChanges {
  @Input() user: User;
  @Input() relation: Relation;
  friendState: RelationSates;

  constructor(private userService: UserService, private authService: AuthService,
              private appEventManagerService: AppEventManagerService, public global: Global) {
  }

  ngOnInit(): void {
    this.getFriendState();
  }


  ngOnChanges(changes: SimpleChanges): void {
    this.getFriendState();
  }

  async addFriend(receiverId: number): Promise<void> {
    await this.userService.addFriend(this.authService.user.id, receiverId);
  }

  async confirmFriend(relationId: number): Promise<void> {
    await this.userService.confirmFriend(this.authService.user.id, relationId);
  }

  async deleteFriend(relationId: number): Promise<void> {
    if (!confirm('Voulez-vous vraiment supprimer cet ami ?')) {
      return;
    }
    await this.userService.deleteFriend(this.authService.user.id, relationId);
  }

  getFriendState(): void {
    if (!this.relation) {
      this.relation = this.userService.getFriend(this.authService.user.friends, this.user.id, this.authService.user.id);
    }
    if (this.userService.isFriend(this.authService.user?.friends, this.user?.id)) {
      if (this.relation.isConfirmed) {
        this.friendState = RelationSates.Confirmed;
      } else {
        this.friendState = RelationSates.NotConfirmed;
      }
    } else {
      this.friendState = RelationSates.NotAdded;
    }
  }

}
