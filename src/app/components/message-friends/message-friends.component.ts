import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Relation} from '../../models/relation';
import {UserService} from '../../services/user.service';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user';
import {ConversationService} from '../../services/conversation.service';
import {Conversation} from '../../models/conversation';
import {AppEventManagerService} from '../../services/app-event-manager.service';
import {Global} from '../../global';
import {MatDialog} from '@angular/material/dialog';
import {ConversationComponent} from '../conversation/conversation.component';

@Component({
  selector: 'app-message-friends',
  templateUrl: './message-friends.component.html',
  styleUrls: ['./message-friends.component.scss']
})
export class MessageFriendsComponent implements OnInit {
  relations: Relation[];
  conversations: Conversation[];
  conversationsIdSelected: number[];

  newConversation: Conversation;
  usersId: number[] = new Array<number>();

  constructor(private userService: UserService, private authService: AuthService,
              private conversationService: ConversationService, private appEventManagerService: AppEventManagerService,
              public global: Global, public dialog: MatDialog) {
    this.newConversation = new Conversation(this.authService.user.id);
  }

  async ngOnInit(): Promise<void> {
    this.conversationsIdSelected = JSON.parse(localStorage.getItem('conversationsId')) ?? new Array<number>();
    this.subscribeToConversations();
    this.subscribeToRelations();
    await this.conversationService.getAllConversation(this.authService.user.id);
    await this.userService.getAllFriends(this.authService.user.id);
    this.appEventManagerService.subscribe('conversationChanged', async () => {
      await this.conversationService.getAllConversation(this.authService.user.id);
    });
  }

  subscribeToRelations(): void {
    this.userService.relationsSubject.subscribe(
      (relations: Relation[]) => {
        this.relations = relations;
      },
      error => {
        console.error(error);
      }
    );
  }

  subscribeToConversations(): void {
    this.conversationService.conversationsSubject.subscribe(
      (conversations) => {
        this.conversations = conversations;
      },
      error => {
        console.error(error);
      }
    );
  }

  getOtherUserInFriend(relation: Relation): User {
    return this.userService.getOtherUserInFriend(relation, this.authService.user.id);
  }

  async createConversation(): Promise<void> {
    await this.conversationService.createConversation(this.newConversation.AdminId, this.newConversation.name, this.usersId);
    this.newConversation = new Conversation(this.authService.user.id);
  }

  selectUser(userId: number): void {
    if (this.isSelected(userId)) {
      this.usersId = this.usersId.filter(id => id !== userId);
    } else {
      this.usersId.push(userId);
    }
  }

  isSelected(userId: number): boolean {
    return this.usersId.some(id => id === userId);
  }

  openConversation(conversation: Conversation): void {
    const modal = this.dialog.open(ConversationComponent, {
      height: '100vh',
      width: '100vw',
      maxWidth: '100vw'
    });
    modal.componentInstance.conversationId = conversation.id;
  }
}
