import {Component, OnInit} from '@angular/core';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {Resource} from '../../models/resource';
import {Subscription} from 'rxjs';
import {ResourcesService} from '../../services/resources.service';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {
  //#region icons
  faPlus = faPlus;

  //#endregion
  resources: Resource[];

  constructor(private resourcesService: ResourcesService, public authService: AuthService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToResources();
    await this.resourcesService.getResourcesByNumberAndDateOrder();
  }

  subscribeToResources(): void {
    this.resourcesService.resourcesSubject.subscribe(
      (resources: Array<Resource>) => {
        this.resources = resources;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
