import {Component, OnInit} from '@angular/core';
import {Resource} from '../../models/resource';
import {ResourcesService} from '../../services/resources.service';
import {AuthService} from '../../services/auth.service';
import {ResourceShared} from '../../models/resource-shared';
import {AppEventManagerService} from '../../services/app-event-manager.service';

@Component({
  selector: 'app-resources-shared',
  templateUrl: './resources-shared.component.html',
  styleUrls: ['./resources-shared.component.scss']
})
export class ResourcesSharedComponent implements OnInit {
  resources: Resource[];
  resourcesShared: ResourceShared[];

  constructor(private resourceService: ResourcesService, private authService: AuthService,
              private appEventManagerService: AppEventManagerService) {
  }

  async ngOnInit(): Promise<void> {
    this.subscribeToResources();
    await this.resourceService.getAllResourcesSharedByUserId(this.authService.user.id);
    this.appEventManagerService.subscribe('resourceShared',
      () => this.resourceService.getAllResourcesSharedByUserId(this.authService.user.id));
  }

  subscribeToResources(): void {
    this.resourceService.resourcesSubject.subscribe(
      (resourcesShared: Array<ResourceShared>) => {
        this.resourcesShared = resourcesShared;
        this.resources = new Array<Resource>();
        for (const resourceShared of resourcesShared) {
          this.resources.push(resourceShared.Resource);
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
