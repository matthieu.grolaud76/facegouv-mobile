import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesSharedComponent } from './resources-shared.component';

describe('ResourcesSharedComponent', () => {
  let component: ResourcesSharedComponent;
  let fixture: ComponentFixture<ResourcesSharedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourcesSharedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesSharedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
