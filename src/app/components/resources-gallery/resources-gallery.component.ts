import {Component, Input, OnInit} from '@angular/core';
import {Resource} from '../../models/resource';
import {ResourceShared} from '../../models/resource-shared';

@Component({
  selector: 'app-resources-gallery',
  templateUrl: './resources-gallery.component.html',
  styleUrls: ['./resources-gallery.component.scss']
})
export class ResourcesGalleryComponent implements OnInit {
  @Input() resources: Resource[];
  @Input() emptyText: string;
  @Input() resourcesShared: ResourceShared[];

  constructor() {
  }

  ngOnInit(): void {
  }

  getSenderResourceShare(resource: Resource): string {
    const sender = this.resourcesShared?.find(value => value.Resource === resource).Sender;
    const receiver = this.resourcesShared?.find(value => value.Resource === resource).Receiver;
    const strSender = sender ? `Partagé par ${sender.firstName} ${sender.lastName}` : '';
    const strReceiver = sender ? ` à ${receiver.firstName} ${receiver.lastName}` : '';
    return strSender + strReceiver;
  }
}
