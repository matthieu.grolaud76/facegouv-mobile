import {Component, Input, OnInit} from '@angular/core';
import {IComment} from '../../models/i-comment';
import {Comment} from '../../models/comment';
import {Answer} from '../../models/answer';
import {CommentTypes} from '../../models/enums/comment-types.enum';
import {AuthService} from '../../services/auth.service';
import {ResourcesService} from '../../services/resources.service';
import {patchTsGetExpandoInitializer} from '@angular/compiler-cli/ngcc/src/packages/patch_ts_expando_initializer';
import {AppEventManagerService} from "../../services/app-event-manager.service";

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.scss']
})
export class CommentFormComponent implements OnInit {
  comment: IComment;
  content: string;
  @Input() source: Array<IComment>;
  @Input() sourceId: number;
  @Input() placeHolder: string;
  @Input() isComment: boolean;

  constructor(private authService: AuthService, private resourceService: ResourcesService,
              private appEventManager: AppEventManagerService) {
  }

  ngOnInit(): void {
    // On récupère le type du commentaire fournis pour instancier notre commentaire/réponse
    if (this.isComment) {
      this.comment = new Comment(this.authService.user);
    } else {
      this.comment = new Answer(this.authService.user);
    }
  }

  async postComment(): Promise<void> {
    if (this.comment.User.id && this.sourceId && this.content) {
      if (this.isComment) {
        await this.resourceService.postComment(this.comment.User.id, this.sourceId, this.content);
      } else {
        await this.resourceService.postSubComment(this.comment.User.id, this.sourceId, this.content);
      }
      this.source.push(this.comment);
      this.appEventManager.broadcast('commentPosted');
      this.content = null;
    }
  }

  isConnected(): boolean {
    return this.authService.isConnected();
  }
}
