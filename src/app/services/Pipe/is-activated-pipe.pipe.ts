import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'isActivatedPipe'
})
export class IsActivatedPipePipe implements PipeTransform {

  transform(value): unknown {
    return value ? 'Oui' : 'Non';
  }

}
