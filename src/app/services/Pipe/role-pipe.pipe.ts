import {Pipe, PipeTransform} from '@angular/core';
import {UserStates} from '../../models/enums/user-states.enum';

@Pipe({
  name: 'rolePipe'
})
export class RolePipePipe implements PipeTransform {

  transform(value): string {
    switch (value) {
      case UserStates.banned:
        return 'Banni';
      case UserStates.visitor:
        return 'Visiteur';
      case UserStates.moderator:
        return 'Modérateur';
      case UserStates.admin:
        return 'Admin';
      case UserStates.superAdmin:
        return 'Super-Admin';
      default:
        return 'Utilisateur';
    }
  }

}
