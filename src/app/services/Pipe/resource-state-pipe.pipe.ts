import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'resourceStatePipe'
})
export class ResourceStatePipePipe implements PipeTransform {

  transform(value): void {
    switch (value) {
      case 1:
        value = 'À valider';
        break;
      case 2:
        value = 'Validé';
        break;
      case 3:
        value = 'Suspendu';
        break;
      default:
        value = 'Restreint';
    }
    return value;
  }

}
