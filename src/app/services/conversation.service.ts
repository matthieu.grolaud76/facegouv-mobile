import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {SocketService} from './socket.service';
import {Global} from '../global';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {MessageConversation} from '../models/messageConversation';
import {Conversation} from '../models/conversation';

@Injectable({
  providedIn: 'root'
})
export class ConversationService {
  conversationsSubject: Subject<Array<Conversation>> = new Subject();
  conversationSubject: Subject<Conversation> = new Subject();

  constructor(private socketService: SocketService, private global: Global,
              private http: HttpClient, private toastr: ToastrService) {
  }

  createConversation(adminId: number, name: string, usersId: number[]): void {
    usersId.push(adminId);
    this.socketService.createConv(adminId, name, usersId);
  }

  async getAllConversation(userId: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}conversations/${userId}`).toPromise();
    this.conversationsSubject.next(data as Conversation[]);
  }

  async getConversation(convId: number): Promise<void> {
    const data = await this.http.get(`${this.global.apiUrl}conversation/${convId}`).toPromise();
    this.conversationSubject.next(data as Conversation);
  }

  async deleteConversation(): Promise<void> {

  }

  async addMessage(message: MessageConversation): Promise<void> {
    this.socketService.addMessageToConv(message.User.id, message.ConversationId, message.Message);
  }
}
